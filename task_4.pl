f(X, Y, F):- X+Y < -1, F is 2*X.
f(X, Y, F):- X+Y >= -1, X+Y =< 1, F is cos(X*Y).
f(X, Y, F):- X+Y > 1, F is sqrt(X+Y).
