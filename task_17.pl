max([X],X) :- !.
max([X|Xs], M):- max(Xs, M), M >= X.
max([X|Xs], X):- max(Xs, M), X >  M.
