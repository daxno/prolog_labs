%PREDICATES
/*
man(symbol).
woman(symbol).
parent(symbol, symbol).
kol_det(symbol, symbol, integer).
mother(symbol).
father(symbol).
*/

%CLAUSES
brother(X, Y):- man(X), parent(P, X), parent(P, Y), X \= Y.
sister(X, Y):- woman(X), parent(P, X), parent(P, Y), X \= Y.
uncle(X, Y):- man(X), brother(X, P), parent(P, Y).
grandma(X, Y):- woman(X), parent(X, P), parent(P, Y).

man("Sasha").
man("Kirill").
woman("Natasha").
parent("Dasha", "Natasha").
parent("Dasha", "Sasha").
parent("Larisa", "Kirill").

%GOAL
/*
brother("Kirill", "Natasha").
brother("Sasha", "Natasha").
*/
