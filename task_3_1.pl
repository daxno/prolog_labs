/* 
a(x) = sqrt(|x|) 
b(y) = cos(y) + 2
f(x, y) = 3*y*a(x) + sin(x*y)*b(y)
*/
a(X, A):- A is sqrt(abs(X)).
b(Y, B):- B is cos(Y) + 2.
f(X, Y, F):- a(X, A), b(Y, B), F is 3*Y*A + sin(X*Y)*B,
			write("a("), write(X), write(") = "), write(A), nl, write("b("), write(Y), write(") = "), write(B).

