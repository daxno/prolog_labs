q(A, B, C, X1, X2):- A = 0, write('Not a quadratic equation'); D is B*B-4*A*C,
         ( 
	   D = 0, X1 is -B/2/A, X2 is -B/2/A;
           D > 0, X1 is (-B+sqrt(D))/2/A, X2 is (-B-sqrt(D))/2/A
	 ).

