is_even(X) :- X mod 2 =:= 0.

sum_even([], 0).
sum_even([H|T], S):- is_even(H), sum_even(T, S1), S is S1+H.
sum_even([H|T], S):- \+ is_even(H), sum_even(T, S).

/*
sum([H|T], S):- sum(T, S1), S is H+S1.
sum_even([], 0).
sum_even(L, S):- findall(X, (member(X, L), X mod 2 =:= 0), L2), sum(L2, S).*/
