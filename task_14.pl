lensp([], 0).
lensp([_|T], L):- lensp(T, Lt), L is Lt + 1.
