num(0, "ноль").
num(1, "один").
num(2, "два").
num(3, "три").
num(4, "четыре").
num(5, "пять").
num(6, "шесть").
num(7, "семь").
num(8, "восемь").
num(9, "девять").

num2word([], []).
num2word([H|T], [NewH|NewT]):- num(H, NewH), num2word(T, NewT).
