%CLAUSES
employee(1, "Zhingalov", "prog", 100000, 1).
employee(2, "Litvinov", "prog", 100000, 1).
employee(3, "Sergeev", "manager", 100000, 2).
employee(4, "Dmitriev", "prog", 100000, 2).
employee(5, "Pavlov", "manager", 100000, 2).
employee(6, "Alekseev", "cook", 10000, 1).
emp_family_status(1, "Litvinova", 0).
emp_family_status(2, "Dmitrieva", 1).
emp_family_status(3, "Sergeeva", 0).
emp_family_status(6, "Alekseeva", 3).

is_familiar(A, B):- employee(Id_a, A, _, _, Dep_a), employee(Id_b, B, _, _, Dep_b), Id_a \= Id_b, Dep_a = Dep_b. 
is_spouse_familiar(A, B):- is_familiar(A, B), employee(Id_a, A, _, _, _), employee(Id_b, B, _, _, _), emp_family_status(Id_a, _, _), emp_family_status(Id_b, _, _).

%GOAL
/*
employee(Id, Sur, Pos, Sal, 1).
employee(Id, Sur, Pos, Sal, Dep), Sal > 10000.
emp_family_status(Id_e, _, Child_numb), employee(Id_e, Sur, Pos, Sal, Dep), Child_numb > 1.
*/

