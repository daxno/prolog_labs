age("Kirill", 20).
age("Sasha", 25).
age("Dima", 17).
age("Nik", 22).
age("Natasha", 21).
age("Katya", 16).

man("Kirill").
man("Sasha").
man("Dima").
man("Nik").
woman("Natasha").
woman("Katya").

hair_color("Kirill", "red").
hair_color("Sasha", "red").
hair_color("Dima", "red").
hair_color("Nik", "black").
hair_color("Natasha", "red").
hair_color("Katya", "black").

cities("Kirill", ["Moscow", "Paris", "LA", "Berlin"]).
cities("Dima", ["London", "Novgorod", "Dmitrov"]).

find_redhead_old_man:- man(_), age(_, Age), Age > 18, hair_color(_, "red"), cities(_, Cities), !, maplist(writeln, Cities).
