worker_info("Яндекс", "Жингалов", 1000000).
worker_info("Mail", "Жингалов", 2000000).
worker_info("ВШЭ", "Петров", 100000).
worker_info("МГУ", "Сидоров", 2).
worker_info("Сбер", "Никоаев", 123).
worker_info("Тинь", "Николаев", 456).
worker_info("B", "Николаев", 987).

lensp([], 0).
lensp([_|T], L):- lensp(T, Lt), L is Lt + 1.
readline(Line):- readln(L), atomics_to_string(L, Line).
readint(Num):- readline(String), atom_number(String, Num).
run:-
write("Введите минимальный оклад:"), nl, readint(Min_salary), worker_info(Company, Sec_name, Salary), Salary > Min_salary, findall(_, worker_info(_, Sec_name, _), Lst), lensp(Lst, L), L >= 2, write(Sec_name).
