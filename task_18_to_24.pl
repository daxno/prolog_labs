/*Task 18*/
/*Task A (implement is_sublist)*/
/*If [3,5] is sublist of [1,2,3,4,5,6]. But more likely it's subset
is_sublist([], []).
is_sublist([_|T1], [_|T2]):- is_sublist(T1, T2).
is_sublist([_|T1], T2):- is_sublist(T1, T2).
*/

/*if not [3,5] is sublist of [1,2,3,4,5,6]*/
is_sublist([], []).
is_sublist([H1|T1], [H2|T2]):- H1 == H2, check(T1, T2).
is_sublist([H1|T1], [H2|T2]):- H1 \= H2, is_sublist([H1|T1], T2).

check([], []).
check([], _).
check([H1|T1], [H2|T2]):- H1 = H2, check(T1, T2).  
check([H1|_], [H2|_]):- H1 \= H2, false.

/*Task B (with append)*/
isin(Sublist, List):- append( [_, Sublist, _], List).


/*Task 19 (intersection of lists)*/
/* Assume that intersection is just elements that appears in both lists.*/
intersection([], _, []).
intersection([H1|T1], L2, [H1|T2]) :- member(H1, L2), intersection(T1, L2, T2).
intersection([_|T1], L2, Res) :- intersection(T1, L2, Res).


/*Task 20*/
list_len([], 0).
list_len([_|T], Len):- list_len(T, Prev_len), Len is Prev_len + 1.

strlen(Str, Len):- string_codes(Str, List), list_len(List, Len).


/*Task 21 (number of symbols in file)*/
readline(Line):- readln(L), atomics_to_string(L, Line).

count_char_in_file(Count):- readline(Fname), ck(Fname, Count).
cc(Filename, _):- \+ exists_file(Filename), write("File not found"), nl.
cc(Filename, Len):- exists_file(Filename), open(Filename, read, Stream), read_stream_to_codes(Stream, List), close(Stream), strlen(List, Len). 

/*Task 22 (number of words in file)*/
% 32 - whitespace.
count_word_in_file(Count):- readline(Fname), open_file(Fname, Count).

open_file(Filename, _):- \+ exists_file(Filename), write("File not found"), nl.
open_file(Filename, Len):- exists_file(Filename), open(Filename, read, Stream), read_stream_to_codes(Stream, List), close(Stream), wc(List, Len).

wc([], 1).
wc([H|T], C):- wc(T, Tmp_c), H = 32, C is Tmp_c + 1.
wc([H|T], C):- wc(T, Tmp_c), H \= 32, C is Tmp_c.


/*Task 23*/
% TODO implement http://www.swi-prolog.org/howto/database.html
/*
tree_search():- readline(Tree_type), found_tree(Tree_type).
found_tree(Tree_type):- consult("tree.dat"), term_string(Term, Tree_type).
*/


/*Task 24*/
% TODO implement
/*
run:- \+ exists_file("tel.dat"), write("База не найдена, создаю новый файл"), open("tel.dat", write, _), nl, run.
run:-  exists_file("tel.dat"), consult("tel.dat"), runyes.
runyes:- readline(Name), readline(Number), assertz(телефон(Name, Number)), nl, 
		write("Продолжить ввод?(yes/no)"), readline(Ans), runans(Ans).
runans(Ans):- Ans="yes", runyes.
runans(Ans):- Ans="no", tell("tel.dat"), told.
*/
