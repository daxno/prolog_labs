fib(0, 0):- !.
fib(1, 1):- !.
fib(K, F):- K1 is K-1, fib(K1, F1), K2 is K-2, fib(K2, F2), F is F1+F2.
