ages("Zhingalov", 20).
ages("Litvinova", 21).
ages("Petrov", 23).
ages("Sidorov", 14).
ages("Ivanov", 13).
man("Zhingalov").
man("Petrov").
man("Sidorov").

old_men:- man(X), ages(X, Age), Age > 18, write(X), nl, fail. 
